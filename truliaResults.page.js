// @ts-check
import { Page } from '../aquifer/Page';
export const truliaResultsPage = new class TruliaResults extends Page {
  constructor() {
    super();  //https://www.trulia.com/p/nc/durham/1920-e-main-st-durham-nc-27703--2159711185

    this.crimeLevelSpan = this.get('#crimeCard span.typeEmphasize');
    super.nameElements();
  }

}();
