// @ts-check
import { Page } from '../aquifer/Page';
export const truliaMainPage = new class TruliaMain extends Page {
  constructor() {
    super(`https://www.trulia.com`);

    this.searchBox = this.get('input#homepageSearchBoxTextInput').tagAsLoadCriterion();
    this.searchButton = this.get('button.homepageSearchButton').tagAsLoadCriterion();
    // this.aYearCell = this.get('//tbody/tr/td[2]');
    // this.anAccountNumberCell = this.get('//tbody/tr/td[3]');
    // this.anOwnersNameCell = this.get('//tbody/tr/td[5]');
    // this.anAmountDueCell = this.get('//tbody/tr/td[9]');
    super.nameElements();
  }

}();
