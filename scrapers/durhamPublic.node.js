var XLSX = require('xlsx')
import * as CSV from 'csv-string';
const cheerio = require('cheerio')
var request = require('sync-request');
import * as rp from 'request-promise';
var fs = require('fs');
var os = require('os');


import * as axios from 'axios';

// var workbook = XLSX.readFile('./other/debtors.xlsx');
var workbook = XLSX.readFile('./other/durham_realproperty_qwxea.xls');
var sheet_name_list = workbook.SheetNames;
var xlDataCsv = XLSX.utils.sheet_to_csv(workbook.Sheets[sheet_name_list[0]]);


function getTopRowInfo($, selector) {
  return $(selector).first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");
}
function getGeneralPropertyInfomation($, text) {
  return $(`strong:contains("${text}")`).first().parent().contents().last().text()
}

const arr = CSV.parse(xlDataCsv);
// console.log("arr")
// console.log(arr)
// console.log("arr[0]")
// console.log(arr[0])



async function postTransactionHistory(ownerid) {
  const res = await rp.post('https://property.spatialest.com/nc/durham-tax/data/getData.php', {
    form: {
      qtype: 'history',
      ownerid
    }
  });
  return res;
}

async function postTaxBills(search_num) {
  const res = await rp.post('https://property.spatialest.com/nc/durham-tax/data/getData.php', {
    form: {
      qtype: 'search_results_byparcel',
      search_num
    }
  });
  return res;
}
// console.log(stuff('168442'));

async function getTaxBills() {

  console.log(JSON.parse(await postTaxBills('168442')).results);
}

console.log('here1')
// getTaxBills();

fs.appendFileSync('other/searchScraped.json', '[' + os.EOL)

getEverything();

async function getEverything() {
  console.log('hereherehrer')

  for (let i = 0; i < arr.length; i++) {
    const x = arr[i];


    // //for delinquent download spreadsheet    
    // const oldOwner = x[0];
    // const newOwner = x[1];
    // const parcelNumber = x[2];
    // const amountOwed = x[3].replace('$', '').replace(',', '').trim();
    // const xlsDownloadData = { oldOwner, newOwner, parcelNumber, amountOwed };


    //for search download spreadsheet:  https://property.spatialest.com/nc/durham/search/1_10000_size:0_100000_assessed:
    const parcelNumber = x[0];
    const location = x[1];
    const owner = x[2];
    const propDescription = x[3];
    const assessedValue = x[4];
    const lotSize = x[5];
    const saleDate = x[6];
    const salePrice = x[7];

    const searchDownloadData = { parcelNumber, location, owner, propDescription, assessedValue, lotSize, saleDate, salePrice };

    console.log('i: ' + i);
    console.log('parcel: ' + parcelNumber);
    // console.log(isNaN(parseInt(parcel)));
    if (!isNaN(parseInt(parcelNumber))) {
      const cardPrintPageUrl = `https://property.spatialest.com/nc/durham/property/${parcelNumber}/print`
      const taxesPageUrl = `https://property.spatialest.com/nc/durham-tax/#/parcel/${parcelNumber}`

      // log.logRichMessages([
      //   { text: '🕸  ', style: log.style.emoji },
      //   { text: 'Load ', style: log.style.verb },
      //   { text: cardPrintPageUrl, style: log.style.selector }], true, false);

      var res = request('GET', cardPrintPageUrl);
      // console.log(res.getBody().toString());
      const html = res.getBody().toString();
      const $ = cheerio.load(html)

      let data = {};
      //$('div div:nth-of-type(2)  ul').first().text()
      //$('div div:nth-of-type(2)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/');
      //$('div div:nth-of-type(2)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");

      // data.ownerName = $('div div div  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");
      // data.ownerAddress = $('div div div:nth-of-type(2)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");
      // data.locationAddress = $('div div div:nth-of-type(3)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");


      // data.oldOwner_dqt = oldOwner_dqt;
      // data.newOwner_dqt = newOwner_dqt;
      // data.parcelNumber_dqt = parcelNumber_dqt;
      // data.amountOwed_dqt = amountOwed_dqt;

      // data.delinquentData = xlsDownloadData;
      data.searchDownloadData = searchDownloadData;

      data.ownerName = getTopRowInfo($, 'div div div ul')
      data.ownerAddress = getTopRowInfo($, 'div div div:nth-of-type(2) ul')
      data.locationAddress = getTopRowInfo($, 'div div div:nth-of-type(3) ul')

      data.parcelNumber = getGeneralPropertyInfomation($, 'Parcel Ref No');
      data.accountNumber = getGeneralPropertyInfomation($, 'Account No');
      data.landUseDesc = getGeneralPropertyInfomation($, 'Land Use Desc');
      data.lastSaleDate = getGeneralPropertyInfomation($, 'Last Sale Date');
      data.lastSalePrice = getGeneralPropertyInfomation($, 'Last Sale Price');
      data.appraisal = getGeneralPropertyInfomation($, 'Property Tax Appraisal');
      data.yearBuilt = getGeneralPropertyInfomation($, 'Year Built');
      data.heatedArea = getGeneralPropertyInfomation($, 'Heated Area');
      data.bathrooms = getGeneralPropertyInfomation($, 'Bathroom(s)');
      data.bedrooms = getGeneralPropertyInfomation($, 'Bedroom(s)');
      data.fireplace = getGeneralPropertyInfomation($, 'Fireplace');
      data.basement = getGeneralPropertyInfomation($, 'Basement');
      data.attachedGarage = getGeneralPropertyInfomation($, 'Attached Garage');
      data.landMarketValue = getGeneralPropertyInfomation($, 'Land Market Value');
      data.landPresentUseValue = getGeneralPropertyInfomation($, 'Land Present Use Value');
      data.landTotalAssessedValue = getGeneralPropertyInfomation($, 'Land Total Assessed Value');
      data.buildingValue = getGeneralPropertyInfomation($, 'Building Value');
      data.mapAcres = getGeneralPropertyInfomation($, 'Map Acres');
      data.appraisalDate = $('.panel-body p').first().text().replace('Appraised Value as of ', '');

      data.taxBills = JSON.parse(await postTaxBills(data.parcelNumber)).results


      //don't use accountNumber from card, above, cos mihgt be missing leading 0's which are necessary to find the transaction histories
      const accountNumber = data.taxBills.find(x => x.TaxYear === '2018').OwnerID;

      data.currentOwnerTransactionHistory = JSON.parse(await postTransactionHistory(accountNumber)).transactions


      fs.appendFileSync('other/searchScraped.json', JSON.stringify(data, null, 4) + ',' + os.EOL)


      // console.log("accountNumber!!!!")
      // console.log(accountNumber)

      console.log(JSON.stringify(data, null, 4));

      console.log('--------------------------------------------------------------------------------------------------')

    }
  }
}