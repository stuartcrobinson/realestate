var XLSX = require('xlsx')
import * as CSV from 'csv-string';
import { UiElement } from '../aquifer/UiElement';
import { taxBillSearchResults, TaxBillSearchResultsPage } from './taxBillSearchResults.page';
import { log } from '../aquifer/AquiferLog';
const cheerio = require('cheerio')
var request = require('sync-request');

const DURHAM_THE_PARLOUR = {  "lat" : 35.9966254,  "lng" : -78.9022573};
const DURHAM_MOTORCO = {  "lat" : 35.9966254,  "lng" : -78.9022573};
const DURHAM_MOTORCO

//TODO how to -- api get crime level per address 

//get parcel numbers from excel download of delinquent properties https://property.spatialest.com/nc/durham-tax/#/delinquents/list
//then get data for each:
// https://property.spatialest.com/nc/durham/property/150126/print
// https://property.spatialest.com/nc/durham-tax/#/parcel/150126

function getTopRowInfo($, selector) {
  return $(selector).first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");
}
function getGeneralPropertyInfomation($, text) {
  return $(`strong:contains("${text}")`).first().parent().contents().last().text()
}

function getDurhamCoordinates(address) {
  return getCoordinatesInCity(address, "Durham, NC");
}
function getCoordinatesInCity(address, cityState = 'Durham, NC') {
  var res = request('GET', `https://maps.googleapis.com/maps/api/geocode/json?address=${address}, ${cityState}&key=AIzaSyC6-Qgoef6S7RXqVZTa1tLWHpY518e40fo`);
  let body = JSON.parse(res.body.toString());
  let coordinates = body.results[0].geometry.location;
  return coordinates;
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}
function getDistanceToDowntownDurham({ lat, lng }) {
  // https://maps.googleapis.com/maps/api/geocode/json?address=117%20Market%20St,%20Durham,%20NC%2027701&key=AIzaSyC6-Qgoef6S7RXqVZTa1tLWHpY518e40fo
  let { lat: dtLat, lng: dtLng } = JSON.parse(`{  "lat" : 35.9966254,  "lng" : -78.9022573}`);  //the parlour 
  let { lat: dtLat, lng: dtLng } = DURHAM_THE_PARLOUR;  //the parlour 

  // console.log("JSON.parse)");
  // console.log(JSON.parse(`{  "lat" : 35.9966254,  "lng" : -78.9022573}`));
  // console.log("lat, lng, dtLat, dtLng");
  // console.log(lat, lng, dtLat, dtLng);

  const km = getDistanceFromLatLonInKm(lat, lng, dtLat, dtLng);



  // console.log("km");
  // console.log(km);

  return km / 1.609;
}

//https://maps.googleapis.com/maps/api/geocode/json?address=1320%20vickers%20durham%20nc&key=AIzaSyC6-Qgoef6S7RXqVZTa1tLWHpY518e40fo


console.log("getDistanceToDowntownDurham");
console.log(getDistanceToDowntownDurham({ lat: 35.9940329, lng: -78.898619 }));


// process.abort();

describe('durham:', () => {

  it('stuff', () => {

    var workbook = XLSX.readFile('./other/debtors.xlsx');
    var sheet_name_list = workbook.SheetNames;
    var xlDataCsv = XLSX.utils.sheet_to_csv(workbook.Sheets[sheet_name_list[0]]);

    const arr = CSV.parse(xlDataCsv);
    // console.log("arr")
    // console.log(arr)
    // console.log("arr[0]")
    // console.log(arr[0])

    arr.forEach(x => {
      // console.log("hi")
      // console.log("x")
      // console.log(x)

      // console.log(x[2]);
      const parcel = x[2];
      console.log('parcel: ' + parcel);
      console.log(isNaN(parseInt(parcel)));
      if (!isNaN(parseInt(parcel))) {
        const cardPrintPageUrl = `https://property.spatialest.com/nc/durham/property/${parcel}/print`
        const taxesPageUrl = `https://property.spatialest.com/nc/durham-tax/#/parcel/${parcel}`

        log.logRichMessages([
          { text: '🕸  ', style: log.style.emoji },
          { text: 'Load ', style: log.style.verb },
          { text: cardPrintPageUrl, style: log.style.selector }], true, false);

        var res = request('GET', cardPrintPageUrl);
        // console.log(res.getBody().toString());
        const html = res.getBody().toString();
        const $ = cheerio.load(html)

        let data = {};
        //$('div div:nth-of-type(2)  ul').first().text()
        //$('div div:nth-of-type(2)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/');
        //$('div div:nth-of-type(2)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");

        // data.ownerName = $('div div div  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");
        // data.ownerAddress = $('div div div:nth-of-type(2)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");
        // data.locationAddress = $('div div div:nth-of-type(3)  ul').first().text().trim().replace(/(?:\r\n|\r|\n)/g, '/').replace(/ +/g, " ");


        data.ownerName = getTopRowInfo($, 'div div div ul')
        data.ownerAddress = getTopRowInfo($, 'div div div:nth-of-type(2) ul')
        data.locationAddress = getTopRowInfo($, 'div div div:nth-of-type(3) ul')



        data.parcelNumber = getGeneralPropertyInfomation($, 'Parcel Ref No');
        data.accountNumber = getGeneralPropertyInfomation($, 'Account No');
        data.landUseDesc = getGeneralPropertyInfomation($, 'Land Use Desc');
        data.lastSaleDate = getGeneralPropertyInfomation($, 'Last Sale Date');
        data.lastSalePrice = getGeneralPropertyInfomation($, 'Last Sale Price');
        data.appraisal = getGeneralPropertyInfomation($, 'Property Tax Appraisal');
        data.yearBuilt = getGeneralPropertyInfomation($, 'Year Built');
        data.heatedArea = getGeneralPropertyInfomation($, 'Heated Area');
        data.bathrooms = getGeneralPropertyInfomation($, 'Bathroom(s)');
        data.bedrooms = getGeneralPropertyInfomation($, 'Bedroom(s)');
        data.fireplace = getGeneralPropertyInfomation($, 'Fireplace');
        data.basement = getGeneralPropertyInfomation($, 'Basement');
        data.attachedGarage = getGeneralPropertyInfomation($, 'Attached Garage');
        data.landMarketValue = getGeneralPropertyInfomation($, 'Land Market Value');
        data.landPresentUseValue = getGeneralPropertyInfomation($, 'Land Present Use Value');
        data.landTotalAssessedValue = getGeneralPropertyInfomation($, 'Land Total Assessed Value');
        data.buildingValue = getGeneralPropertyInfomation($, 'Building Value');
        data.mapAcres = getGeneralPropertyInfomation($, 'Map Acres');
        // data.parcelNumber = getGeneralPropertyInfomation($, 'asdf');
        // data.parcelNumber = getGeneralPropertyInfomation($, 'asdf');
        // data.parcelNumber = getGeneralPropertyInfomation($, 'asdf');
        // data.parcelNumber = getGeneralPropertyInfomation($, 'asdf');
        // data.parcelNumber = getGeneralPropertyInfomation($, 'asdf');
        data.appraisalDate = $('.panel-body p').first().text().replace('Appraised Value as of ', '');
        //.panel-body p

        // console.log($('strong:contains("Land Use")').first().parent().contents().last().text())
        // console.log($('strong:contains("Map Acres")').first().parent().text())
        // console.log("taxesPageUrl")
        // console.log(taxesPageUrl)


        let taxBillSearchResults = new TaxBillSearchResultsPage(parcel);
        // taxBillSearchResults.loadingSpinner.waitForNotExist();

        taxBillSearchResults.load_waitForChange('//tbody');
        taxBillSearchResults.loadingSpinner.waitForNotExist();

        // console.log(taxBillSearchResults.aYearCell.getTexts());
        // console.log(taxBillSearchResults.anAccountNumberCell.getTexts());
        // console.log(taxBillSearchResults.anOwnersNameCell.getTexts());
        // console.log(taxBillSearchResults.anAmountDueCell.getTexts());

        const taxData = [
          taxBillSearchResults.aYearCell.getTexts(),
          taxBillSearchResults.anAccountNumberCell.getTexts(),
          taxBillSearchResults.anOwnersNameCell.getTexts(),
          taxBillSearchResults.anAmountDueCell.getTexts()
        ];
        // console.log(taxData);

        data.taxData = taxData;



        let coordinates = getDurhamCoordinates(data.locationAddress);
        let distanceToDowntownDurham = getDistanceToDowntownDurham(coordinates);

        data.lat = coordinates.lat;
        data.lng = coordinates.lng;
        data.distParlour = Number.parseFloat(distanceToDowntownDurham).toPrecision(3);

        console.log(JSON.stringify(data, null, 4));

        // browser.url(taxesPageUrl)
        // console.log(taxBillSearchResults.topAccountInfo.getText());
        // console.log(new UiElement('//tbody/tr/td[3]').getText());
        console.log('--------------------------')
      }
    });


  });
});
