import { truliaMainPage } from "./truliaMain.page";
import { truliaResultsPage } from "./truliaResults.page";

describe('trulia:', () => {

  it('stuff', () => {

    truliaMainPage.load(400000);
    truliaMainPage.searchBox.clear();
    truliaMainPage.keys('1920 e main durham nc');
    truliaMainPage.searchButton.click_waitForNotExisting();
    truliaResultsPage.crimeLevelSpan.hover();
    console.log(truliaResultsPage.crimeLevelSpan.getText());
  });
});