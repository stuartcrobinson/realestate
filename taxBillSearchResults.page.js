// @ts-check
import { Page } from '../aquifer/Page';
export class TaxBillSearchResultsPage extends Page {
  constructor(parcel) {
    super(`https://property.spatialest.com/nc/durham-tax/#/parcel/${parcel}`);

    this.topAccountInfo = this.get('//tbody/tr/td[3]');
    this.loadingSpinner = this.get('.spinner.show');
    this.aYearCell = this.get('//tbody/tr/td[2]');
    this.anAccountNumberCell = this.get('//tbody/tr/td[3]');
    this.anOwnersNameCell = this.get('//tbody/tr/td[5]');
    this.anAmountDueCell = this.get('//tbody/tr/td[9]');
    super.nameElements();
  }

  getYears() {
    ////tbody/tr/td[2]

    return
  }
  getAccountNumbers() {
    ////tbody/tr/td[3]
  }
  getOwnersNames() {
    ////tbody/tr/td[5]
  }
  getAmountsDue() {
    //// tbody/tr/td[9]
  }
};
