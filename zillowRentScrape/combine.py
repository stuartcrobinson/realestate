import os, json

# path = 'zillowRentScrape/zillowScrapedSamples'
# path = 'zillowScrapedSamples'
path = '/Users/stuartrobinson/zillow/Downloads'
files = os.listdir(path)



files_txt = [i for i in files if i.endswith('.json')]

ar = []

for file in files_txt:
    # read file
    file = path + '/' + file
    with open(file, 'r') as myfile:
        data = myfile.read()
        data = data.replace('\n', '').replace('\r', '')
        # data = data.replace('\n', ' -- ').replace('\r', ' -- ')
        # parse file
        obj = json.loads(data)
        ar = ar + obj

# with open("combined.json", "w") as write_file:
#     json.dump(ar, write_file)

import json
import csv

# # input data
# json_file = open("data.json", "r")
# json_data = json.load(json_file)
# json_file.close()

# data = json.loads(json_data)

data = ar

tsv_file = open("data.csv", "w")
tsv_writer = csv.writer(tsv_file, delimiter='\t')



tsv_writer.writerow(data[0].keys()) # write the header

for row in data: # write data rows
    tsv_writer.writerow(row.values())

tsv_file.close()
